<?php 

namespace PayPal\Rest;
namespace App\Service;

use PayPal\Api\Item;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use App\Entity\Shipping;
use PayPal\Api\ItemList;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use PayPal\Api\RedirectUrls;
use App\Service\SessionService;
use PayPal\Api\PaymentExecution;
use App\Service\ShoppingCartService;
use PayPal\Auth\OAuthTokenCredential;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Paypal{

public function __construct(SessionService $session, UrlGeneratorInterface $router,
RequestStack $requestStack, ShoppingCartService $shoppingCart, \Swift_Mailer $mailer){

    $this->session = $session->session;
    $this->router = $router;
    $this->requestStack = $requestStack;
    $this->shoppingCart = $shoppingCart; 
    $this->mailer = $mailer;
    
}
public function  createPayment(){
    
    $payer = new Payer();
    $payer->setpaymentMethod('paypal');

    foreach($this->session->get('cart') as $product){
        $items[] = $item = new Item();
        $item->setName($product['name'])
        ->setCurrency('EUR')
        ->setQuantity($product['amount'])->setPrice($product['price']);
    }


    $itemList = new ItemList();
    $itemList->setItems($items);
    
    $details = new Details();
    $details->setShipping(0)
    ->setTax(0)
    ->setSubTotal($this->session->get('cart_data/total_price'));

    $amount = new Amount();
    $amount->setCurrency('EUR')
    ->setTotal($this->session->get('cart_data/total_price'))
    ->setDetails($details);

    $transaction = new Transaction();
    $transaction->setAmount($amount)
    ->setItemList($itemList)
    ->setDescription("Payment Description")
    ->setInvoiceNumber(uniqid());

    $redirectUrls = new RedirectUrls();
    $redirectUrls->setReturnUrl($this->router->generate('execute_payment', [], UrlGeneratorInterface::ABSOLUTE_URL))
    ->setCancelUrl($this->router->generate('shop', [], UrlGeneratorInterface::ABSOLUTE_URL));

    $payment = new Payment();
    $payment->setIntent("sale")
    ->setPayer($payer)
    ->setRedirectUrls($redirectUrls)
    ->setTransactions(array($transaction));

    $apiContext = new ApiContext(
        new OAuthTokenCredential(
            $_ENV['PAYPAL_CLIENT_ID'],
            $_ENV['PAYPAL_CLIENT_SECRET']
        )
        );

        $payment->create($apiContext);
        $approvalUrl = $payment->getApprovalLink();
        return $approvalUrl;
}


public function executePayment($em)
    {
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $_ENV['PAYPAL_CLIENT_ID'],     // ClientID
                $_ENV['PAYPAL_CLIENT_SECRET']      // ClientSecret
            )
        );

        $request = $this->requestStack->getCurrentRequest();

        $paymentId = $request->query->get('paymentId');
        $payment = Payment::get($paymentId, $apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($request->query->get('PayerID'));

        $transaction = new Transaction();
        $amount = new Amount();
        $details = new Details();

        $details->setShipping(0)
            ->setTax(0)
            ->setSubtotal($this->session->get('cart_data/total_price'));
        $amount->setCurrency('EUR');
        $amount->setTotal($this->session->get('cart_data/total_price'));
        $amount->setDetails($details);
        $transaction->setAmount($amount);
        $execution->addTransaction($transaction);

        $result = $payment->execute($execution, $apiContext);
        $result = $result->toJSON();
        $result = json_decode($result);

        if($result->state === 'approved'){

            $order = new Shipping();
            $total = $result->transactions[0]->amount->total;
            $date = new \DateTime($result->create_time);
            $products = $result->transactions[0]->item_list;
            $currency = $result->transactions[0]->amount->currency;
            $uniqueId = md5(rand());

            $tabItem = [];
            foreach ($products as $item){
                array_push($tabItem, $item);
            }
            $order->setEmail($result->payer->payer_info->email);
            $order->setPrice((int) $total);
            $order->setCurrency($currency);
            $order->setProducts($tabItem[0]);
            $order->setCreatetime($date);
            $order->setUniqueId($uniqueId);
            $order->setFinish(false);
            $shipping = [
            'name' =>  $result->transactions[0]->item_list->shipping_address->recipient_name,
            'line1' => $result->transactions[0]->item_list->shipping_address->line1,
            'city' => $result->transactions[0]->item_list->shipping_address->city,
            'state' => $result->transactions[0]->item_list->shipping_address->state,
            'postal_code' =>  $result->transactions[0]->item_list->shipping_address->postal_code,
            'country_code' => $result->transactions[0]->item_list->shipping_address->country_code, 
            ];
            $order->setShip($shipping);
           
           $em->persist($order);
           $em->flush();
           
            $this->shoppingCart->deleteCart();
        }
    }


}

