<?php

namespace App\Service;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ApiConnectRepository")
 */
class MailChimpApiConnect
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */

    private $api;
    /**
     * @ORM\Column(type="string", length=255)
     */

    private $idList;
    /**
     * @ORM\Column(type="string", length=255)
     */

    public function __construct(){
        $this->idList = '7586fd4cbc';
        $this->api =  $api = [
            'login' => '',
            'key' => 'b6d7ef277a00b355ad13990e1ebdb723-us4',
            'url'   => 'https://us4.api.mailchimp.com/3.0/',
        ];
    }

    function mc_request($api, $type, $target, $data = null){
      
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api['url']. $target);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$api['key'])));
        //	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10 );
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/3.0' );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
        $response = curl_exec( $ch );
        curl_close($ch);
        
        return json_decode($response, true);
    }

    /**
     * Get the value of idList
     */ 
    public function getIdList()
    {
        return $this->idList;
    }

    /**
     * Set the value of idList
     *
     * @return  self
     */ 
    public function setIdList($idList)
    {
        $this->idList = $idList;

        return $this;
    }

    /**
     * Get the value of api
     */ 
    public function getApi()
    {
        return $this->api;
    }

    /**
     * Set the value of api
     *
     * @return  self
     */ 
    public function setApi($api)
    {
        $this->api = $api;

        return $this;
    }
}
