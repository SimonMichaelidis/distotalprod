<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LabelRepository")
 */
class Label
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $datecreation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Groupmusic", mappedBy="label")
     */
    private $groupmusics;


    /**
     * @ORM\Column(type="text")
     */
    private $paraOne;

    /**
     * @ORM\Column(type="string",  nullable=true, length=255)
     */
    private $pictureNewsletter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $paraTwo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bandcamp;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $instagram;

    public function __construct()
    {
        $this->groupmusics = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function getDatecreation(): ?\DateTimeInterface
    {
        return $this->datecreation;
    }

    public function setDatecreation(\DateTimeInterface $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    /**
     * @return Collection|Groupmusic[]
     */
    public function getGroupmusics(): Collection
    {
        return $this->groupmusics;
    }

    public function addGroupmusic(Groupmusic $groupmusic): self
    {
        if (!$this->groupmusics->contains($groupmusic)) {
            $this->groupmusics[] = $groupmusic;
            $groupmusic->setLabel($this);
        }

        return $this;
    }

    public function removeGroupmusic(Groupmusic $groupmusic): self
    {
        if ($this->groupmusics->contains($groupmusic)) {
            $this->groupmusics->removeElement($groupmusic);
            // set the owning side to null (unless already changed)
            if ($groupmusic->getLabel() === $this) {
                $groupmusic->setLabel(null);
            }
        }

        return $this;
    }


    public function getParaOne(): ?string
    {
        return $this->paraOne;
    }

    public function setParaOne(string $paraOne): self
    {
        $this->paraOne = $paraOne;

        return $this;
    }

    public function getPictureNewsletter(): ?string
    {
        return $this->pictureNewsletter;
    }

    public function setPictureNewsletter(string $pictureNewsletter): self
    {
        $this->pictureNewsletter = $pictureNewsletter;

        return $this;
    }

    public function getParaTwo(): ?string
    {
        return $this->paraTwo;
    }

    public function setParaTwo(?string $paraTwo): self
    {
        $this->paraTwo = $paraTwo;

        return $this;
    }

    public function getBandcamp(): ?string
    {
        return $this->bandcamp;
    }

    public function setBandcamp(?string $bandcamp): self
    {
        $this->bandcamp = $bandcamp;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }


}
