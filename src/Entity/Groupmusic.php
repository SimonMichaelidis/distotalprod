<?php

namespace App\Entity;
use App\Entity\Album;
use App\Entity\Concert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupmusicRepository")
 */
class Groupmusic
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $datecreation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Label", inversedBy="groupmusics")
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Album", mappedBy="groupmusic")
     */
    private $albums;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Concert", mappedBy="groupes")
     */
    private $concerts;

   /**
     * @ORM\Column(type="string")
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $albumiframe;

    /**
     * @ORM\Column(type="text")
     */
    private $paraOne;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bandcamp;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $instagram;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Soundtrack", inversedBy="groupmusics")
     */
    private $extrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Font", inversedBy="groupmusics")
     */
    private $font;



 
    public function __construct()
    {
        $this->albums = new ArrayCollection();
        $this->concerts = new ArrayCollection();
    }

    public function __toString(){
        return 
        $this->getName();
    }
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function getDatecreation(): ?\DateTimeInterface
    {
        return $this->datecreation;
    }

    public function setDatecreation(\DateTimeInterface $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    public function getLabel(): ?Label
    {
        return $this->label;
    }

    public function setLabel(?Label $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
            $album->setGroupmusic($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->albums->contains($album)) {
            $this->albums->removeElement($album);
            // set the owning side to null (unless already changed)
            if ($album->getGroupmusic() === $this) {
                $album->setGroupmusic(null);
            }
        }
    return $this;
    }

    /**
     * @return Collection|Concert[]
     */
    public function getConcerts(): Collection
    {
        return $this->concerts;
    }

    public function addConcert(Concert $concert): self
    {
        if (!$this->concerts->contains($concert)) {
            $this->concerts[] = $concert;
            $concert->addGroupe($this);
        }

        return $this;
    }

    public function removeConcert(Concert $concert): self
    {
        if ($this->concerts->contains($concert)) {
            $this->concerts->removeElement($concert);
            $concert->removeGroupe($this);
        }

        return $this;
    }

    public function getPicture()
    {
        return $this->picture;
    }

    public function setPicture($picture)
    {
        $this->picture = $picture;

        return $this;
    }

    public function getJson(){
    return json_encode(get_object_vars($this));
    }

    public function getClip(): ?string
    {
        return $this->clip;
    }

    public function setClip(?string $clip): self
    {
        $this->clip = $clip;

        return $this;
    }

    public function getAlbumiframe(): ?string
    {
        return $this->albumiframe;
    }

    public function setAlbumiframe(?string $albumiframe): self
    {
        $this->albumiframe = $albumiframe;

        return $this;
    }

    public function getParaOne(): ?string
    {
        return $this->paraOne;
    }

    public function setParaOne(string $paraOne): self
    {
        $this->paraOne = $paraOne;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getBandcamp(): ?string
    {
        return $this->bandcamp;
    }

    public function setBandcamp(?string $bandcamp): self
    {
        $this->bandcamp = $bandcamp;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

  

    public function getExtrait(): ?Soundtrack
    {
        return $this->extrait;
    }

    public function setExtrait(?Soundtrack $extrait): self
    {
        $this->extrait = $extrait;

        return $this;
    }


    public function removeFont(Font $font): self
    {
        if ($this->font->contains($font)) {
            $this->font->removeElement($font);
        }

        return $this;
    }



    /**
     * Get the value of font
     */ 
    public function getFont()
    {
        return $this->font;
    }

    /**
     * Set the value of font
     *
     * @return  self
     */ 
    public function setFont($font)
    {
        $this->font = $font;

        return $this;
    }



}
