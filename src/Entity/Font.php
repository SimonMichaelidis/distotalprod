<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FontRepository")
 */
class Font
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $googleFontUrl;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $css;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Groupmusic", mappedBy="font")
     */
    private $groupmusic;

   

 

    public function __construct()
    {
        $this->groupmusic = new ArrayCollection();
        $this->groupmusics = new ArrayCollection();
   
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGoogleFontUrl(): ?string
    {
        return $this->googleFontUrl;
    }

    public function setGoogleFontUrl(string $googleFontUrl): self
    {
        $this->googleFontUrl = $googleFontUrl;

        return $this;
    }

    public function getCss(): ?string
    {
        return $this->css;
    }

    public function setCss(string $css): self
    {
        $this->css = $css;

        return $this;
    }

    /**
     * @return Collection|Groupmusic[]
     */
    public function getGroupmusic(): Collection
    {
        return $this->groupmusic;
    }

    public function addGroupmusic(Groupmusic $groupmusic): self
    {
        if (!$this->groupmusic->contains($groupmusic)) {
            $this->groupmusic[] = $groupmusic;
            $groupmusic->setFont($this);
        }

        return $this;
    }





}
