<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SoundPositionRepository")
 */
class SoundPosition
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Soundtrack", inversedBy="soundPositions")
     */
    private $soundtrack;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Album", inversedBy="soundPositions" )
     */
    private $album;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getSoundtrack(): ?Soundtrack
    {
        return $this->soundtrack;
    }

    public function setSoundtrack(?Soundtrack $soundtrack): self
    {
        $this->soundtrack = $soundtrack;

        return $this;
    }

    public function getAlbum(): ?Album
    {
        return $this->album;
    }

    public function setAlbum(?Album $album): self
    {
        $this->album = $album;

        return $this;
    }
}
