<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlbumRepository")
 */
class Album
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $releaseat;

    /**
     * @ORM\Column(type="text")
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picturealt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Groupmusic", inversedBy="albums")
     */
    private $groupmusic;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SoundPosition", mappedBy="album")
     */
    private $soundPositions;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bandcampUrl;

    public function __construct(){
        $this->soundtracks = new ArrayCollection();
        $this->soundPositions = new ArrayCollection();
    }

    public function __toString(){
             return   $this->getname();
    }        
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getReleaseat(): ?\DateTimeInterface
    {
        return $this->releaseat;
    }

    public function setReleaseat(\DateTimeInterface $releaseat): self
    {
        $this->releaseat = $releaseat;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getPicturealt(): ?string
    {
        return $this->picturealt;
    }

    public function setPicturealt(string $picturealt): self
    {
        $this->picturealt = $picturealt;

        return $this;
    }

    public function getGroupmusic(): ?Groupmusic
    {
        return $this->groupmusic;
    }

    public function setGroupmusic(?Groupmusic $groupmusic): self
    {
        $this->groupmusic = $groupmusic;

        return $this;
    }

    /**
     * @return Collection|SoundPosition[]
     */
    public function getSoundPositions(): Collection
    {
        return $this->soundPositions;
    }

    public function addSoundPosition(SoundPosition $soundPosition): self
    {
        if (!$this->soundPositions->contains($soundPosition)) {
            $this->soundPositions[] = $soundPosition;
            $soundPosition->setAlbum($this);
        }

        return $this;
    }

    public function removeSoundPosition(SoundPosition $soundPosition): self
    {
        if ($this->soundPositions->contains($soundPosition)) {
            $this->soundPositions->removeElement($soundPosition);
            // set the owning side to null (unless already changed)
            if ($soundPosition->getAlbum() === $this) {
                $soundPosition->setAlbum(null);
            }
        }

        return $this;
    }

    public function getBandcampUrl(): ?string
    {
        return $this->bandcampUrl;
    }

    public function setBandcampUrl(?string $bandcampUrl): self
    {
        $this->bandcampUrl = $bandcampUrl;

        return $this;
    }

    

   
}
