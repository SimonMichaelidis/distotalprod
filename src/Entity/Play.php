<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayRepository")
 */
class Play
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Artist", inversedBy="plays")
     */
    private $artist;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Instrument", inversedBy="plays")
     */
    private $instrument;

   
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Soundtrack", inversedBy="plays", cascade={"remove"})
     */
    private $soundtrack;

    public function __construct()
    {
        $this->soundtracks = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArtist(): ?Artist
    {
        return $this->artist;
    }

    public function setArtist(?Artist $artist): self
    {
        $this->artist = $artist;

        return $this;
    }

    public function getInstrument(): ?Instrument
    {
        return $this->instrument;
    }

    public function setInstrument(?Instrument $instrument): self
    {
        $this->instrument = $instrument;

        return $this;
    }

    public function getSoundtrack(): ?Soundtrack
    {
        return $this->soundtrack;
    }

    public function setSoundtrack(?Soundtrack $soundtrack): self
    {
        $this->soundtrack = $soundtrack;

        return $this;
    }


    
}
