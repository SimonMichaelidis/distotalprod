<?php

namespace App\Entity;

use App\Entity\Groupmusic;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConcertRepository")
 */
class Concert {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $city;

     /**
     * @ORM\Column(type="string", length=150)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $place;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $cp;

    /**
     * @ORM\Column(type="text")
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picturealt;


    /**
     * @ORM\Column(type="datetime")
     */
    private $dateconcert;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Groupmusic", inversedBy="concerts")
     */
    private $groupes;

    public function __construct()
    {
        $this->groupes = new ArrayCollection();
    }

  
    

   
    public function __toString(){
        return $this->getName();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getPictureAlt(): ?string
    {
        return $this->picturealt;
    }

    public function setPicturealt(string $picturealt): self
    {
        $this->picturealt = $picturealt;

        return $this;
    }

    public function getDateconcert(): ?\DateTimeInterface
    {
        return $this->dateconcert;
    }

    public function setDateconcert(\DateTimeInterface $dateconcert): self
    {
        $this->dateconcert = $dateconcert;

        return $this;
    }

    /**
     * @return Collection|Groupmusic[]
     */
    public function getGroupes(): Collection
    {
        return $this->groupes;
    }

    public function addGroupe(Groupmusic $groupe): self
    {
        if (!$this->groupes->contains($groupe)) {
            $this->groupes[] = $groupe;
        }

        return $this;
    }

    public function removeGroupe(Groupmusic $groupe): self
    {
        if ($this->groupes->contains($groupe)) {
            $this->groupes->removeElement($groupe);
        }

        return $this;
    }

 

   

   
    
}
