<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupemusicRepository")
 */
class Groupemusic
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Font", inversedBy="groupmusicsfontalbum")
     */
    private $fontalbumname;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFontalbumname(): ?Font
    {
        return $this->fontalbumname;
    }

    public function setFontalbumname(?Font $fontalbumname): self
    {
        $this->fontalbumname = $fontalbumname;

        return $this;
    }
}
