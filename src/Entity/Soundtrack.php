<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SoundtrackRepository")
 */
class Soundtrack
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $number;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Play", mappedBy="soundtrack", cascade={"remove"})
     */
    private $plays;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $soundFileName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SoundPosition", mappedBy="soundtrack", cascade={"remove"})
     */
    private $soundPositions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Groupmusic", mappedBy="extrait")
     */
    private $groupmusics;

    
    public function __construct()
    {
        $this->plays = new ArrayCollection();
        $this->soundPositions = new ArrayCollection();
        $this->groupmusics = new ArrayCollection();
    }

    public function __toString(){ 
        return $this->getName();
    } 

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAlbum(): ?Album
    {
        return $this->album;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getDuration(): ?\DateTimeInterface
    {
        return $this->duration;
    }

    public function setDuration(\DateTimeInterface $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return Collection|Play[]
     */
    public function getPlays(): Collection
    {
        return $this->plays;
    }

    public function addPlay(Play $play): self
    {
        if (!$this->plays->contains($play)) {
            $this->plays[] = $play;
            $play->setSoundtrack($this);
        }

        return $this;
    }

    public function removePlay(Play $play): self
    {
        if ($this->plays->contains($play)) {
            $this->plays->removeElement($play);
            // set the owning side to null (unless already changed)
            if ($play->getSoundtrack() === $this) {
                $play->setSoundtrack(null);
            }
        }

        return $this;
    }

    public function getSoundFileName(): ?string
    {
        return $this->soundFileName;
    }

    public function setSoundFileName(string $soundFileName): self
    {
        $this->soundFileName = $soundFileName;

        return $this;
    }

    /**
     * @return Collection|SoundPosition[]
     */
    public function getSoundPositions(): Collection
    {
        return $this->soundPositions;
    }

    public function addSoundPosition(SoundPosition $soundPosition): self
    {
        if (!$this->soundPositions->contains($soundPosition)) {
            $this->soundPositions[] = $soundPosition;
            $soundPosition->setSoundtrack($this);
        }

        return $this;
    }

    public function removeSoundPosition(SoundPosition $soundPosition): self
    {
        if ($this->soundPositions->contains($soundPosition)) {
            $this->soundPositions->removeElement($soundPosition);
            // set the owning side to null (unless already changed)
            if ($soundPosition->getSoundtrack() === $this) {
                $soundPosition->setSoundtrack(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Groupmusic[]
     */
    public function getGroupmusics(): Collection
    {
        return $this->groupmusics;
    }

    public function addGroupmusic(Groupmusic $groupmusic): self
    {
        if (!$this->groupmusics->contains($groupmusic)) {
            $this->groupmusics[] = $groupmusic;
            $groupmusic->setExtrait($this);
        }

        return $this;
    }

    public function removeGroupmusic(Groupmusic $groupmusic): self
    {
        if ($this->groupmusics->contains($groupmusic)) {
            $this->groupmusics->removeElement($groupmusic);
            // set the owning side to null (unless already changed)
            if ($groupmusic->getExtrait() === $this) {
                $groupmusic->setExtrait(null);
            }
        }

        return $this;
    }


}
