<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsletterRepository")
 */
class Newsletter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email_address;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get the value of email_address
     */ 
    public function getEmailAddress()
    {
        return $this->email_address;
    }

    /**
     * Set the value of email_address
     *
     * @return  self
     */ 
    public function setEmailAddress($email_address)
    {
        $this->email_address = $email_address;

        return $this;
    }
}
