<?php

namespace App\DataFixtures;

use App\Entity\News;
use App\Entity\Album;
use App\Entity\Label;
use App\Entity\Artist;
use App\Entity\Concert;
use App\Entity\Groupmusic;
use App\Entity\Instrument;
use App\Entity\Soundtrack;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture{
    public function load(ObjectManager $manager){



        for ($i = 0; $i < 10; $i++) {
            $instru = new Instrument();
            $instru->setName('Instrument n°'.$i);
            $manager->persist($instru);
        }

        for ($i = 0; $i < 10; $i++) {
            $artist = new Artist();
            $TabArtist[] = $artist;
            $artist->setName(' Doe'.($i + 1));
            $artist->setFirstName('John');
            $artist->setBirthday(new \DateTime('1991-06-19'));
            $manager->persist($artist);
        }

        for ($i = 0; $i < 1; $i++) {
            $label = new Label();
            $tabLabel[] = $label;
            $label->setName('Distotal Prod');
            $label->setDatecreation(new \DateTime('2018-06-19'));
            $label->setLogo('groupe1-5d73a99bc20d2.jpeg');
            $label->setParaOne('Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia molestiae velit ex, quia id expedita, eveniet maxime eum rerum deleniti, sequi illum soluta cupiditate qui animi dolorum saepe modi numquam?');
            $label->setParaTwo('Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia molestiae velit ex, quia id expedita, eveniet maxime eum rerum deleniti, sequi illum soluta cupiditate qui animi dolorum saepe modi numquam?');
            $label->setParaThree('Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia molestiae velit ex, quia id expedita, eveniet maxime eum rerum deleniti, sequi illum soluta cupiditate qui animi dolorum saepe modi numquam?');
            $manager->persist($label);
        }

        for ($i = 0; $i < 5; $i++) {
            $groupe = new Groupmusic();
            $tabGroupe[] = $groupe;
            $groupe->setName('Groupe n°'.($i + 1));
            $groupe->setDescribes('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime, id quae? Consequatur exercitationem dolor cupiditate quaerat nostrum iste ea, magnam facere dignissimos dolorem praesentium aspernatur deserunt ad harum accusantium voluptate.');
            $groupe->setDatecreation(new \DateTime('1991-06-19'));
            $groupe->setLabel($label);
            $groupe->setPicture('groupe1-5d73a99bc20d2.jpeg');
            $manager->persist($groupe);
        }

        for($i = 0; $i < 10; $i++){
            $soundtrack = new Soundtrack();
            $tabSoundtracks = [];
            $soundtrack->setName('Titre Test');
            $soundtrack->setNumber($i);
            $soundtrack->setDuration(new \DateTime('0'.$i.':02'));
            $manager->persist($soundtrack);

        }
        for($i = 0; $i < 4; $i++){
            $news = new News();
            $tabNews= [];
            $news->setText('nostrum iste ea, magnam facere dignissimos dolorem praesentium aspernatur deserunt ad harum accusantium voluptate.');
            $news->setPicture('groupe1-5d73a99bc20d2.jpeg');
            $news->setTitle('News n°'.$i);
            $news->setEventDate(new \DateTime('202'.$i.'-05-19 03:04:02'));
            $manager->persist($news);

        }

        for ($i = 0; $i < 5; $i++) {
            $album = new Album();
            $tabAlbum[] = $album; 
            $album->setName('Album n°'.($i + 1));
            $album->setReleaseat(new \DateTime('1991-06-19'));
            $album->setPicture('groupe1-5d73a99bc20d2.jpeg');
            $album->setPicturealt('Alt');
            $album->addArtist($artist);
            $album->setGroupmusic($groupe);
            $album->addSoundtrack($soundtrack);
            $manager->persist($album);
        
        }

        for ($i = 0; $i < 9; $i++) {
            $concert = new Concert();
            $tabConcert[] = $concert;
            $concert->setName('concert n°'.($i + 1));
            $concert->setCity('Strasbourg n°'.($i + 1));
            $concert->setPlace('20'.$i.' Avenue de Colmar');
            $concert->setCp('6'.$i.'000');
            $concert->setPicture('groupe1-5d73a99bc20d2.jpeg');
            $concert->addGroupe($groupe);
            $concert->setDateconcert(new \DateTime('2020-05-19 03:04:02'));
            $concert->setPictureAlt('Alt');
            $manager->persist($concert);
        }

       
        $manager->flush();

    }   
}


