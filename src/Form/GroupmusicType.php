<?php

namespace App\Form;

use App\Entity\Font;
use App\Entity\Label;
use App\Entity\Groupmusic;
use App\Entity\Soundtrack;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class GroupmusicType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('name', TextType::class, ['label' => 'Nom du groupe', 'attr' => ['class' => "form-control", 'id' => "inputDefault",  "rows" => "2" ]])
            ->add('paraOne', TextareaType::class, ['label' => 'Description', 'attr' => ['class' => "form-control", 'id' => "exampleTextarea",  "rows" => "3" ]])
            ->add('datecreation', BirthdayType::class, [
                'label' => 'Date de création',
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd'
                ])
            ->add('picture', FileType::class, [
                'attr' =>[
                    'multiple' => 'multiple',
                    'id' => 'preview',
                    'onmousedown' => 'return false',
                    'onkeydown' => 'return false'
                ],
                'label' => 'Photo (BackGround Concerts)',
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid files format (PNG or JPG)',
                    ])
                ],
            ]) 
            ->add('extrait', EntityType::class,[ 
                'required' => false,
                'label' => 'Extrait',
                'class' => Soundtrack::class,
                'choice_label' => function ($sound){
                    return $sound->getName();
                    },
              ])
           
            ->add('facebook', UrlType::class, [
                'label' => 'Facebook',
                'required' => false 
            ])      
            ->add('bandcamp', UrlType::class, [
                'label' => 'Bandcamp',
                'required' => false
            ])      
            ->add('instagram', UrlType::class, [
                'label' => 'Instagram',
                'required' => false
            ])      
            ->add('label', EntityType::class,[ 
                'label' => 'Label',
                'class' => Label::class,
                'choice_label' => function ($label){
                    return $label->getName();
                    },
              ])
            ->add('font', EntityType::class,[ 
                'required' => false,
                'label' => 'Police - Nom du groupe',
                'class' => Font::class,
                'choice_label' => function ($font){
                    return $font->getName();
                    },
              ])
          
            ->add('save', SubmitType::class, [
                'label' => 'Valider',
                'attr' => ['class' => 'btn btn-primary']])
                ;


            
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Groupmusic::class,
        ]);
    }
}
