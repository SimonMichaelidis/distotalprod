<?php

namespace App\Form;

use App\Entity\News;
use App\Entity\Soundtrack;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Title *'

            ])
            ->add('clip', TextType::class, [
                'label' => 'Clip',
                'required' => false
            ])
            ->add('picture', FileType::class, [
                'label' => 'Background *',
                'data_class' => null,
                'constraints' => [
                    new File([
                        'maxSize' => '5024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid files format (PNG or JPG)',
                    ])
                ],
            ])
            ->add('bandcamp', UrlType::class, [
                'label' => 'Bandcamp',
            ])
            ->add('choice', ChoiceType::class, [
                'label' => "Extrait ? *",
                'expanded' => true,
                'mapped' => false, 
                'choices'  => [
                    'Yes' => true,
                    'No' => false,
                ],
            ])
            ->add('sound', EntityType::class,[ 
                'label' => 'Sound',
                'class' => Soundtrack::class,
                'choice_label' => function ($sound){
                    return $sound->getName();
                    },      
                ])
            ->add('text', TextareaType::class, [
                'label' => 'News *'
            ])
            ->add('eventdate', BirthdayType::class, [
                'format' => 'yyyy-MM-dd',
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'label' => 'Date de l\'évênement',
                // 'required' => false
            ])
            ->add('save', SubmitType::class)
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}
