<?php
namespace App\Form;

use App\Entity\Label;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class LabelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('name', TextType::class)
            ->add('datecreation', BirthdayType::class,[
                'required' => false,
                'format' => 'yyyy-MM-dd',
                'widget' => 'single_text',
            ])
            ->add('paraOne', TextareaType::class, [
            ])
            ->add('paraTwo', TextareaType::class, [
            ])
            ->add('facebook', UrlType::class, [
                'label' => "Facebook",
                'required' => false
            ])
            ->add('bandcamp', UrlType::class, [
                'label' => "Bandcamp ",
                'required' => false
            ])
            ->add('instagram', UrlType::class, [
                'label' => "Instagram ",
                'required' => false
            ])
            ->add('picture', FileType::class, [
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid files format (PNG or JPG)',
                    ])
                ],
            ])
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Label::class,
        ]);
    }
}
