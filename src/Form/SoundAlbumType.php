<?php

namespace App\Form;

use App\Entity\Album;
use App\Entity\Soundtrack;
use App\Entity\SoundPosition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class SoundAlbumType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', IntegerType::class)
            ->add('soundtrack', EntityType::class,[
                'label' => 'Soundtrack',
                'class' => Soundtrack::class,
                'choice_label' => function ($soundtrack){
                    return $soundtrack->getName();
                }])
            ->add('album', EntityType::class,[
                'label' => 'Album',
                'class' => Album::class,
                'choice_label' => function ($album){
                    return $album->getName();
                }])
                ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SoundPosition::class,
        ]);
    }
}
