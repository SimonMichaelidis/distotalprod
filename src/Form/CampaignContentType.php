<?php

namespace App\Form;

use App\Entity\Campaign;
use App\Entity\Template;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CampaignContentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
    $builder
        ->add('campaign_id', EntityType::class, [
                'label' => 'Campaign',
                'class' => Campaign::class,
                'choice_label' => function ($campaign){
                    return $campaign->getName();
                },
                'choice_value' => function ($campaign = null) {
                    return $campaign ? $campaign->get() : '';
                },
        ])
        ->add('id', EntityType::class, [
            'label' => 'Template',
            'class' => Template::class,
            'choice_label' => function ($template){
                return $template->getName();
            },
            'choice_value' => function ($template = null) {
                return $template ? $template->getTemplateId() : '';
            },
        ])

        ->add('title', TextType::class, [
            'label' => 'Title',
            'required' => false
        ])

        ->add('main_content', TextareaType::class, [
            'label' => 'Main',
            'required' => false
        ])
        ->add('aside_content', TextareaType::class, [
            'label' => 'Aside',
            'required' => false
        ]) 
        ->add('save', SubmitType::class);
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
