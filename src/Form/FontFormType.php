<?php

namespace App\Form;

use App\Entity\Font;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class FontFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom - Ex : Lobster'
            ])
            ->add('googleFontUrl', UrlType::class,[
                'label' => 'Link - Ex : https://fonts.googleapis.com/css?family=Lobster&display=swap'
            ])
            ->add('css', TextType::class,[
                'label' => 'CSS - Ex : Lobster'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Valider'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Font::class,
        ]);
    }
}
