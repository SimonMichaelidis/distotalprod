<?php

namespace App\Form;

use App\Entity\Album;
use App\Entity\Groupmusic;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;

class AlbumType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('name', TextType::class, ['label' => 'Titre'])
            ->add('releaseat', BirthdayType::class, [
                'label' => 'Sortie',
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd'
                ])
            ->add('picture', FileType::class, [
                
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid files format (PNG or JPG)',
                    ])
                ],
                ])
            ->add('picturealt',TextType::class, ['label' => 'picture / Texte altérnatif'])
            ->add('bandCampUrl',UrlType::class, ['label' => 'Bandcamp Url'])
            ->add('groupmusic', EntityType::class,[
                'label' => 'Groupe',
                'class' => Groupmusic::class,
                'choice_label' => function ($groupmusic){
                    return $groupmusic->getName();
                }     
                ])
            ->add('save', SubmitType::class, [
                'label' => 'Valider'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Album::class,
        ]);
    }
}
