<?php

namespace App\Form;

use App\Entity\Play;
use App\Entity\Artist;
use App\Entity\Instrument;
use App\Entity\Soundtrack;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PlayType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('artist', EntityType::class, [
                'label' => 'Artist',
                'class' => Artist::class,
                'choice_label' => function ($artist){
                return $artist->getName().''.$artist->getFirstName();
                }
            ])
            ->add('instrument', EntityType::class,[
                'label' => 'Instrument',
                'class' => Instrument::class,
                'choice_label' => function ($groupmusic){
                    return $groupmusic->getName();
                }     
            ])
            ->add('soundtrack', EntityType::class,[
                'label' => 'Soundtrack',
                'class' => Soundtrack::class,
                'choice_label' => function ($groupmusic){
                    return $groupmusic->getName();
        
                }
                ])
            ->add('save', SubmitType::class, [
                'label' => 'Envoyer'
            ]);

     
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Play::class,
        ]);
    }
}
