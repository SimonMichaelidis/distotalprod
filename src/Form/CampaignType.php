<?php

namespace App\Form;

use App\Entity\Template;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CampaignType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('subject', TextType::class)
            ->add('preview_text', TextType::class)
            // ->add('id', EntityType::class, [
            //     'label' => 'Template',
            //     'class' => Template::class,
            //     'choice_label' => function ($template){
            //         return $template->getName();
            //     },
            //     'choice_value' => function ($template = null) {
            //         return $template ? $template->getTemplateId() : '';
            //     },
            // ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
