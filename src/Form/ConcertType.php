<?php

namespace App\Form;

use App\Entity\Concert;
use App\Entity\Groupmusic;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ConcertType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('name', TextType::class, ['label' => "Evênement"])
            ->add('city', TextType::class, ['label' => 'Ville'])
            ->add('place', TextType::class, ['label' => 'Adresse'])
            ->add('cp', TextType::class, ['label' => 'Code Postal'])
            ->add('dateconcert', BirthdayType::class, [
                'label' => 'Date du concert', 
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('picture', FileType::class, [
                'label' => "Affiche de l'évênement",
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '5024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid files format (PNG or JPG)',
                    ])
                ],
            ])
            ->add('picturealt', TextType::class, ['label' => 'Texte alternatif'])
            ->add('groupes',  CollectionType::class,[
                'entry_type' => EntityType::class,
                'entry_options' => [ 'class' => Groupmusic::class],
                "allow_add" => true,
                "allow_delete" => true,
                "by_reference" => false,
                "required" => false,
                'label' => false
                    ])
            ->add('save', SubmitType::class, [
                'label' => 'Valider', 'attr' => ['class' => 'btn btn-outline-success', 'style' => 'float:right;']])
        ;
    }
    


  

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Concert::class,
        ]);
    }
}
