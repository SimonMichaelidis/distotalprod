<?php

namespace App\Form;

use App\Entity\Soundtrack;
use App\Form\SoundtrackType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SoundtrackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Titre'])
            ->add('soundFileName', FileType::class, [
                'label' => 'Mp3',
                'data_class' => null,
                'constraints' => [
                    new File([
                        'maxSize' => '900M',
                        'mimeTypes' => [
                            'audio/mpeg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid files format (.mp3)',
                    ])
                ],
            ])  
            ->add('save', SubmitType::class, [
                'label' => 'Valider'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Soundtrack::class,
        ]);
    }
}
