<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CommandeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userfname', TextType::class, [ 
                'mapped' => false,
                'attr' => [
                'placeholder' => 'Prénom *'
                ]])
            ->add('userlname', TextType::class, [ 
                'mapped' => false,
                'attr' => [
                'placeholder' => 'Nom *'
                ]])
            ->add('city', TextType::class, [ 
                'mapped' => false,
                'attr' => [
                'placeholder' => 'Ville *'
                ]])
            ->add('street', TextType::class, [
                'mapped' => false,
                'attr' => [
                'placeholder' => 'Rue/Voie *'
                ]])
            ->add('number', NumberType::class, [
                'mapped' => false,
                'attr' => [
                'placeholder' => 'Numéro *'
                ]])
            ->add('cp', NumberType::class, [ 'attr' => [
                'placeholder' => 'Code Postal *'
                ]])
            ->add('cpadresse', TextType::class, [ 'attr' => [
                'placeholder' => 'Complément d\'adresse',
                'required' => false
                ]])
            ->add('Valider', SubmitType::class, ['attr' => [
                'type' => 'button'
            ]

            ])   
                ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);
    }
}
