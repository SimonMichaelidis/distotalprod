<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
    $builder
    ->add('name', TextType::class, [
        'label' => 'Product'
    ])
    ->add('describes', TextareaType::class, [
        'label' => 'Describes' 
        ])
    ->add('price', NumberType::class, [
        'label' =>  'Price'
        ])
    ->add('qty', IntegerType::class, [
        'label' => 'Quantity'
        ])
    ->add('category', EntityType::class, [
        'label' => 'Category',
        'class' => Category::class,
        'choice_label' => function ($category){
            return $category->getName();
        }])
    ->add('picture', FileType::class, [
            'label' => 'Background (PNG) *',
            'data_class' => null,
            'constraints' => [
                new File([
                    'maxSize' => '5024k',
                    'mimeTypes' => [
                        'image/png',
                        'image/jpeg',
                    ],
                    'mimeTypesMessage' => 'Please upload a valid files format (PNG or JPG)',
                ])
            ],
        ])
        ->add('save', SubmitType::class, [
            'label' => 'Submit'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
