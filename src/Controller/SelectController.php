<?php

namespace App\Controller;



use DateTime;

use App\Entity\News;
use App\Entity\Album;
use App\Entity\Label;
use App\Entity\Concert;
use App\Entity\Groupmusic;
use App\Entity\Newsletter;
use App\Form\NewsletterType;
use App\Service\MailChimpApiConnect;
use Symfony\Component\Finder\Finder;
use App\Controller\HaveALoginController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SelectController extends HaveALoginController{
    
    /**
     * @Route("/", name="home_index")
     */
    public function showOnePage(ObjectManager $em,Request $request, ParameterBagInterface $params, AuthenticationUtils $authenticationUtils){

        $label = $em->getRepository(Label::class)->findOneBy(['name' => 'Distotal Prod' ]);
        $allNews = $em->getRepository(News::class)->findActiveNews(new DateTime('-80 day'));
        $groupes = $em->getRepository(Groupmusic::class)->findBy([], ['name' => 'ASC']);
        $concertB = $em->getRepository(Concert::class)->findConcertBeforeNow();
        $concertA = $em->getRepository(Concert::class)->findConcertAfterNow();
       

        $path = $params->get('upload_directory'); // Path à passer au JS // 
    
        // JSON à passer au JS // 
        $jsonPictureNews = json_encode($label->getPictureNewsletter());
        $json = $this->JsonBackground($allNews);
        $jsonArtist = $this->JsonBackground($groupes); 
        $jsonConcert = $this->JsonBackground($concertA);
        $jsonConcertP = $this->JsonBackground($concertB);

        $formLogin = $this->showLogin($request, $authenticationUtils);
        $news = new Newsletter();
        $form = $this->createForm(NewsletterType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $mchimp = new MailChimpApiConnect();
            $api = $mchimp->getApi();
            $target =  "lists/".$mchimp->getIdList()."/members";
            $data = [
                'email_address' => $form['email_address']->getData(),
                'send_welcome'  => true,
                'status' => 'subscribed',  
            ];
            $mchimp->mc_request($api, 'POST', $target, $data);
            $em->persist($news);
            $em->flush();

        return $this->redirectToRoute('home_index'); 
        } 

        return $this->render('select/artists.html.twig', [
            'label' => $label,
            'groupes' => $groupes,
            'jsonArtist' => $jsonArtist,
            'news' =>   $allNews,
            'pictureNews' => $jsonPictureNews,
            'jsonNews' => $json,
            'concertA' =>   $concertA,
            'concertB' =>   $concertB,
            'jsonConcert' => $jsonConcert,
            'jsonConcertP' => $jsonConcertP,
            'path' => $path,
            'form' => $form->createView(),
            'formLogin' => $formLogin->createView(),
        ]);
    }

    /**
     * @Route("/album/{id}", name="find_one_album")
     * @Route("/albums/", name="find_all_albums")
     */
    public function showAllAlbums(Album $al = null, ObjectManager $em, ParameterBagInterface $params){
        
        $albums = $this->getDoctrine()->getManager()
        ->getRepository(Album::class)
        ->findAll();

        if (!empty($al)){ 
          array_unshift($albums, $al);
        }

        $tab = [];
        foreach($albums as $album){
            array_push($tab, $album->getPicture()); 
        } 
        $json = json_encode($tab);
        $path = $params->get('upload_directory'); 
      
        return $this->render('select/albums.html.twig', [
            'title' => 'Albums',
            'albums' => $albums,
            'json' => $json,
            'path' => $path
        ]);
    }

     /**
     * @Route("/shop/payment", name="payment_paypal")
     */
    public function Payment(){

           if (isset($_COOKIE['product'])) {
                unset($_COOKIE['product']);
                setcookie('product', '', time() - 3600, '/'); 
            }
        return $this->render('select/payment.html.twig');
    }

    /**
     * @Route("/mentions_légales", name="politique")
     */
    public function politique(){
        return $this->render('select/politique.html.twig', [
            'title' => 'Mentions Légales'
        ]);
    }

    
    /**
     * @Route("/mailchimp/unsubscribe/member/{id}", name="unsubscribe_member", methods={"POST"})
     */
    public function del(Request $request , $id = null){

        if ($id == null){
            $id = $request->request->get('email');
        } else {
            $id = $id;
        }
        $mchimp = new MailChimpApiConnect();
        $api = $mchimp->getApi();
        $target =  "lists/".$mchimp->getIdList()."/members/".md5($id);
        $mchimp->mc_request($api, 'DELETE', $target);
        return $this->redirectToRoute('add_contact');
        }

        public function JsonBackground($tab){
            $pictures = []; 
            foreach($tab as $newsJson){
                array_push($pictures, $newsJson->getPicture());
            } 
            $json = json_encode($pictures);
            return $json;
    
        }   

}



   

