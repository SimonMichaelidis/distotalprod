<?php

namespace App\Controller;

use App\Entity\Campaign;
use App\Entity\Template;
use App\Form\CampaignType;
use App\Form\TemplateType;
use App\Form\NewsletterType;
use App\Form\CampaignContentType;
use App\Service\MailChimpApiConnect;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


    /**
     * @Route("/admin")
     */

class MailChimpController extends AbstractController
{
  
public function getList(){

    $mchimp = new MailChimpApiConnect();
    $api = $mchimp->getApi();
    $target =  "lists/".$mchimp->getIdList(); //  Get information about a specific list
    $result = $mchimp->mc_request($api, 'GET', $target);
    return $result;
}

public function getListMembers(){

    $mchimp = new MailChimpApiConnect();
    $api = $mchimp->getApi();
    $target =  "lists/".$mchimp->getIdList()."/members/"; //  Get information about a specific list
    $result = $mchimp->mc_request($api, 'GET', $target);
    return $result;
}

public function getAllCampaigns(){

    $mchimp = new MailChimpApiConnect();
    $api = $mchimp->getApi();
    $target =  "campaigns"; //  Get information about a specific list
    $result = $mchimp->mc_request($api, 'GET', $target);
    return $result;
}

public function getOneCampaing($id){

    $mchimp = new MailChimpApiConnect();
    $api = $mchimp->getApi();
    $target =  "campaigns".$id; //  Get information about a specific list
    $result = $mchimp->mc_request($api, 'GET', $target);
    return $result;

}

public function getAllTemplates(){

    $mchimp = new MailChimpApiConnect();
    $api = $mchimp->getApi();
    $target =  "templates"; //  Get information about a specific list
    $result = $mchimp->mc_request($api, 'GET', $target);
    return $result;

}

public function getOneTemplate($id){
    $mchimp = new MailChimpApiConnect();
    $api = $mchimp->getApi();
    $target = "templates/".$id; //  Get information about a specific list
    $result = $mchimp->mc_request($api, 'GET', $target);
    return $result;
}

public function getCampaignContent($id){
    $mchimp = new MailChimpApiConnect();
    $api = $mchimp->getApi();
    $target = "campaigns/".$id."/content"; //  Get information about a specific list
    $result = $mchimp->mc_request($api, 'GET', $target);
    return $result;
}


// =================================================== POST =================================================== //



/**
 * @Route(methods={"GET","PUT"})
 */
public function mailChimpPut($target, $data){
     $mchimp = new MailChimpApiConnect();
     $api = $mchimp->getApi();
     $result = $mchimp->mc_request($api, 'PUT', $target, $data);
     return $this->redirectToRoute('add_campaign');
}

/**
 * @Route("/mailchimp/campaign/add/content", name="add_campaign_content")
 */
public function putContentCampaign(Request $request){
    
    $form = $this->createForm(CampaignContentType::class);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()){
        $target = "campaigns/".$form['campaign_id']->getData()->getCampaignId()."/content";
        $data = [
            'template' => [
                'id' => intval($form['id']->getData()->getTemplateId()),
                'sections' => [
                    'title' => $form['title']->getData(),
                    'main_content' => $form['main_content']->getData(),
                    'aside_content' => $form['aside_content']->getData(),
                ]
            ]
        ];
        $this->mailChimpPut($target, $data);
    }
    return $this->render('/admin/mailchimp/campaign/content.html.twig', [
         'title' => 'Content',
        'listsC' => $this->getAllCampaigns(),
        'listsT' => $this->getAllTemplates(),
        'form' => $form->createView()
    ]);

}

    /**
     * @Route("/mailchimp/members/", name="add_contact")
     */
    public function members(Request $request){
                
        $form = $this->createForm(NewsletterType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()){
            $mchimp = new MailChimpApiConnect();
            $api = $mchimp->getApi();
            $target =  "lists/".$mchimp->getIdList()."/members/". md5($form['email_address']->getData());
            $data = [
                'email_address' => $form['email_address']->getData(),
                'send_welcome'  => true,
                'status' => 'subscribed', 
            ];
            $mchimp->mc_request($api, 'PUT', $target, $data);
        return $this->redirectToRoute('add_contact');
            }

    return $this->render('/admin/mailchimp/list.html.twig', [
        'title' => 'Contact',
        'lists' => $this->getListMembers(),
        'form' => $form->createView()
    
    ]);
    }

    /**
     * @Route("/mailchimp/campaign/", name="add_campaign")
     */

    public function campaign(Request $request, ObjectManager $em){

        $form = $this->createForm(CampaignType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()){
            $mchimp = new MailChimpApiConnect();
            $api = $mchimp->getApi();
            $target =  "campaigns";
            $data = [
                'type' => "regular",
                'recipients' => [
                    'list_id' => $mchimp->getIdList(),
                ],
                'settings' =>[
                    'title' => $form['title']->getData(),
                    'subject_line' => $form['subject']->getData(),
                    'from_name' => 'Distotal Prod',
                    'reply_to' => ' michaelidis.simon06@gmail.com',
                    'preview_text' => $form['preview_text']->getData(),
                ],
            ];   
            $mchimp->mc_request($api, 'POST', $target, $data);
            $this->aPiIdCampaignToDb($em);
            return $this->redirectToRoute('add_campaign');
            }

    return $this->render('admin/mailchimp/campaign.html.twig', [
        'title' => 'Campaign',
        'lists' => $this->getAllCampaigns(),
        'form' => $form->createView()
    ]);
    }

    /**
     * @Route("/mailchimp/template/", name="add_template")
     */
    public function templates(Request $request, ObjectManager $em){
        
        $form = $this->createForm(TemplateType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $mchimp = new MailChimpApiConnect();
            $api = $mchimp->getApi();
            $target =  "template";
            $data = [
                'name' => $form['name']->getData(),
                'html' => $form['html']->getData(), 
            ];

            $mchimp->mc_request($api, 'POST', $target, $data);
            $em->persist($template);
            $em->flush();
            
            $this->apiIdTemplateToDb($em);
            return $this->redirectToRoute('add_campaign');
            }

    return $this->render('admin/mailchimp/template.html.twig', [
        'title' => 'Template',
        'lists' => $this->getAllTemplates(),
        'form' => $form->createView()
    ]);
    }
    

// ================================================== SEND =============================================================
    
    /**
     * @Route("/mailchimp/test/campaign/{id}", name="test_campaign")
     */
    public function testCampaign($id){
        $mchimp = new MailChimpApiConnect();
        $api = $mchimp->getApi();
        $target = "campaigns/".$id."/actions/test";
        $data = [
            'test_emails' => [
                'michaelidis.simon91@gmail.com'
            ],
            'send_type' => 'html'
        ];

        $result = $mchimp->mc_request($api, 'POST', $target, $data);
        return $this->redirectToRoute('add_campaign');
    }

    /**
     * @Route("/mailchimp/send/campaign/{id}", name="send_campaign")
     */
    public function sendCampaign(){

        $mchimp = new MailChimpApiConnect();
        $api = $mchimp->getApi();
        $target = "campaigns/".$id."actions/send";
        $result = $mchimp->mc_request($api, 'POST', $target);
    }

    // ===================================== MAJ ====================================// 


      /**
     * @Route("/mailchimp/template/maj", name="maj_template")
     */
    public function apiIdTemplateToDb(ObjectManager $em){
        
        $TabArray = [];
        $array = $this->getAllTemplates();

        $templates = $this->getDoctrine()->getRepository(Template::class)->findAll();
        foreach($templates as $template){
            $em->remove($template);
            $em->flush();
        }
       
        foreach($array as $templates => $value){
            array_push($TabArray, $value);
        }
        foreach($TabArray[0] as $key => $value){
            $temp = new Template();
            $temp->setTemplateId($value['id']);
            $temp->setName($value['name']);
            $em->persist($temp);
            $em->flush();
        }                
        return $this->redirectToRoute('add_template');
    }
      /**
     * @Route("/mailchimp/campaign/maj", name="maj_campaign")
     */
    public function aPiIdCampaignToDb(ObjectManager $em){
        
        $TabArray = [];
        $array = $this->getAllCampaigns();
    
        $campaigns = $em->getRepository(Campaign::class)->findAll();
        foreach($campaigns as $campaign){
            $em->remove($campaign);
            $em->flush();
        }
        foreach($array as $campaigns => $value){
            array_push($TabArray, $value);
        }
        foreach($TabArray[0] as $key => $value){
            $camp = new Campaign();
            $camp->setCampaignId($value['id']);
            $camp->setName($value['settings']['title']);
            $em->persist($camp);
            $em->flush();
        }                
        return $this->redirectToRoute('add_campaign');
    }
    



// ===================================================== DELETE ========================================================


    /**
     * @Route("/mailchimp/delete/member/{id}", name="del_member")
     */
    public function delMemberPermanently($id){
    $mchimp = new MailChimpApiConnect();
    $api = $mchimp->getApi();
    $target =  "lists/".$mchimp->getIdList()."/members/".md5($id)."/actions/delete-permanent";
    $mchimp->mc_request($api, 'POST', $target);
    return $this->redirectToRoute('add_contact');
    }

    /**
     * @Route("/mailchimp/delete/member/{id}", name="del_member")
     */
    public function delMember(){
    $id = $this->getUser()->getEmail();
    $mchimp = new MailChimpApiConnect();
    $api = $mchimp->getApi();
    $target =  "lists/".$mchimp->getIdList()."/members/".md5($id);
    $mchimp->mc_request($api, 'DELETE', $target);
    return $this->redirectToRoute('add_contact');
    }

    /**
     * @Route("/mailchimp/delete/campaign/{id}", name="del_campaign" , methods={"GET"})
     */
    public function delCampaign(Request $request){
    $id = $request->attributes->get('id');
    $mchimp = new MailChimpApiConnect();
    $api = $mchimp->getApi();
    $target =  "campaigns/".$id;
    $result = $mchimp->mc_request($api, 'DELETE', $target);
    return $this->redirectToRoute('add_campaign');
    }
    /**
     * @Route("/mailchimp/delete/templates/{id}", name="del_template" , methods={"GET"})
     */
    public function delTemplate(Request $request){
    $id = $request->attributes->get('id');
    $mchimp = new MailChimpApiConnect();
    $api = $mchimp->getApi();
    $target =  "templates/".$id;
    $result = $mchimp->mc_request($api, 'DELETE', $target);
    return $this->redirectToRoute('add_template');
    }




        /**
     * @Route("/mailchimp/member/delete/{id}", name="delete_mc_member")
     */
    public function delMembers(Request $request){

        $id = $request->request->get('id');
        dd($id);
                
        $mchimp = new MailChimpApiConnect();
        $api = $mchimp->getApi();
        $target =  "lists/".$mchimp->getIdList()."/members/". md5($_POST["email"]);
        $data = null;

        $mchimp->mc_request($api, 'DELETE', $target, $data);
        return $this->redirectToRoute('home_index');
        }


}


// List

    // GET /lists Get information about all lists
    // GET /lists/{list_id} Get information about a specific list  

//  List / members 

    // POST /lists/{list_id}/members
    // GET /lists/{list_id}/members 	Get information about members in a list
    // DELETE /lists/{list_id}/members/{subscriber_hash} 

//  Campaign 

    // POST /campaigns 	Create a new campaign
    // GET /campaigns 	Get all campaigns
    // GET /campaigns/{campaign_id} 	Get information about a specific campaign
    // PATCH /campaigns/{campaign_id} 	Update the settings for a campaign
    // DELETE /campaigns/{campaign_id} 	Delete a campaign
    // POST /campaigns/{campaign_id}/actions/send 	Send a campaign
    // POST /campaigns/{campaign_id}/actions/test 	Send a test email

    // Template 
    // POST /templates 	Create a new template
    // GET /templates 	Get all templates
    // GET /templates/{template_id} 	Get information about a specific template
    // PATCH /templates/{template_id} 	Update an existing template
    //  DELETE /templates/{template_id} 	Delete a specific template






