<?php 

namespace App\Controller\Admin;

use Dompdf\Dompdf;
use App\Entity\Font;
use App\Entity\News;
use App\Entity\Play;
use App\Entity\Album;
use App\Entity\Label;
use App\Entity\Artist;
use App\Form\NewsType;
use App\Form\PlayType;
use App\Entity\Concert;
use App\Form\AlbumType;
use App\Form\LabelType;
use App\Form\ArtistType;
use App\Form\ConcertType;
use App\Entity\Groupmusic;
use App\Entity\Instrument;
use App\Entity\Soundtrack;
use App\Form\FontFormType;
use App\Form\GroupmusicType;
use App\Form\InstrumentType;
use App\Form\SoundAlbumType;
use App\Form\SoundtrackType;
use App\Entity\SoundPosition;
use App\Service\FileUploader;
use App\Service\SoundUploader;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


/** 
 * @Route("/admin") 
 */
class AdminController extends AbstractController {

    /**
     * @Route("/", name="panel_admin")
     */
    public function index(Request $request, ObjectManager $em) {

        return $this->redirectToRoute('add_news');
            
    }
  
    /**
     * @Route("/label/add", name="add_label")
     * @Route("/label/update/{id}", name="update_label")
     */
    public function label (Label $label = null, Request $request, ObjectManager $em,  ParameterBagInterface $params, FileUploader $fileUploader) {

        $label1 = $em->getRepository(Label::class)
        ->findOneBy([]);
    
        $label ? 
        $delPictureNewsletter = $label->getPictureNewsletter() :
        $delPictureNewsletter = null && $label = new Label();
        
        $path = $params->get('upload_directory'); // Path pour Js
        $jsonPictureNewsletter = json_encode($label1->getPictureNewsletter()); // Json pour Js
        
        $form = $this->createForm(LabelType::class, $label);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
    
           if ($pictureNewsletter = $form['picture']->getData()){
               if (!is_null($delPictureNewsletter) && ($pictureNewsletter !== $delPictureNewsletter)){
                   $this->delFile('pictures', $delPictureNewsletter);
               }
           }; 
            $pictureNewsletter = $fileUploader->upload($pictureNewsletter);
            $label->setName(ucfirst($form['name']->getData()));
            $label->setParaOne(ucfirst($form['paraOne']->getData()));
            $label->setParaTwo(ucfirst($form['paraTwo']->getData()));
            $label->setFacebook($form['paraTwo']->getData());
            $label->setBandcamp($form['paraTwo']->getData());
            $label->setInstagram($form['paraTwo']->getData());

            $label->setPictureNewsletter($pictureNewsletter);
            
            $em->persist($label);
            $em->flush();


        return $this->redirectToRoute('add_label');
        }
    
        return $this->render('admin/label/label.html.twig', [
            'title' => 'Label',
            'label' => $label1,
            'form' => $form->createView(),
            'pictureNews' => $jsonPictureNewsletter,
            'path' => $path
            ]);
    }

    /**
     * @Route("/artist/add", name="add_artist")
     * @Route("/artist/update/{id}", name="update_artist")
     */
    public function createArtist(Artist $artist = null, Request $request, ObjectManager $em){

        $artists =  $em->getRepository(Artist::class)
        ->findAll(); 

        if(!$artist) {
            $artist = new Artist();
        }

        $form = $this->createForm(ArtistType::class, $artist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $artist->setName(ucfirst($artist->getName()));
            $em->persist($artist);
            $em->flush();

        return $this->redirectToroute('add_artist');
        }

        return $this->render('admin/artist/artist.html.twig', [
            'title' => 'Artiste',
            'form' => $form->createView(),
            'artists' => $artists,
        ]);
    }

     /**
     * @Route("/album/add", name="add_album")
     * @Route("/album/update/{id}", name="update_album")
     */
    public function createAlbum(Album $album = null, Request $request, 
    ObjectManager $em, ParameterBagInterface $params, FileUploader $fileUploader){

        $albums =  $em->getRepository(Album::class)->findAll(); // Récupèration des albums
        $json = $this->JsonBackground($albums);  // Récupèration du nom des images à passer à la vue en format JSON
        $path = $params->get('upload_directory'); // Récupèration du chemin d'accès vers le répértoire des uploads

        if (!$album) { $del_file = null; $album = new Album(); // Gestion de l'édition de l'objet 
        } else { $del_file = $album->getPicture();
        }
    
        $form = $this->createForm(AlbumType::class, $album); // Appel du formulaire
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()){  // Test de validation du formulaire
            $pictureFile = $form['picture']->getData(); 
            if ($del_file !== null){
            $this->delFile('pictures', $del_file); // Suppression de l'image en base afin de ne pas garder sur le serveur des uploads inutiles.
            }

            $picture = $fileUploader->upload($pictureFile); // Image dans le dossier des uploads
            $album->setPicture($picture);   // Envoie du nom de l'image avec id unique en base.
            $album->setName(ucfirst($album->getName())); 
            $em->persist($album); 
            $em->flush(); 
            return $this->redirectToroute('add_album');
        }
        
        return $this->render('admin/album/album.html.twig', [
            'title' => 'Album',
            'form' => $form->createView(),
            'albums' => $albums,
            'json' => $json,
            'path' => $path
        ]);
    }
    
     /**
     * @Route("/news/add", name="add_news")
     * @Route("/news/update/{id}", name="update_news")
     */
    public function createNews(News $news = null, Request $request, ObjectManager $em, FileUploader $fileUploader, ParameterBagInterface $params){

        $allNews  =  $em->getRepository(News::class)
        ->findAll();
        $json = $this->JsonBackground($allNews); // convert all pictures to Json
        $path = $params->get('upload_directory'); // path to JS
        /* End part JS */ 
        if (!$news) { $del_file = null; $news = new News();;
            } else { $del_file = $news->getPicture();
            }
            
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $pictureFile = $form['picture']->getData();
            if (!is_null($del_file)){ 
                $this->delFile('pictures', $del_file);
            }
            $picture = $fileUploader->upload($pictureFile);
            $news->setPicture($picture); 
            $news->setTitle(ucfirst($news->getTitle()));
            $news->setText(ucfirst($news->getText()));
            if($form['choice']->getData() == true){
                $news->setSound($news->getSound());
            } else {
                $news->setSound(null);
            }
            $news->setClip($news->getClip());
            $em->persist($news);
            $em->flush();
            return $this->redirectToroute('add_news');
        }
        return $this->render('admin/news/news.html.twig', [
            'title' => 'News',
            'form' => $form->createView(),
            'news' => $allNews, 
            'json' => $json,
            'path' => $path
        ]);
    }

    /**
     * @Route("/groupe/add", name="add_groupe")
     * @Route("/groupe/update/{id}", name="update_groupe")
     */
    public function addGroup(Groupmusic $groupe = null, Request $request, 
    ObjectManager $em, FileUploader $fileUploader, ParameterBagInterface $params){
        
        $groupes =  $em->getRepository(Groupmusic::class)->findAll();
        $json = $this->JsonBackground($groupes); 
        $path = $params->get('upload_directory');

        if(!$groupe){ $del_file = null; $groupe = new Groupmusic(); // Edition 
        } else { $del_file = $groupe->getPicture();
        }
        $form = $this->createForm(GroupmusicType::class, $groupe);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $pictureFile = $form['picture']->getData();
            if (!is_null($del_file)){
                $this->delFile('pictures', $del_file);
            }
            $picture = $fileUploader->upload($pictureFile);
            $groupe->setPicture($picture);
            $groupe->setParaOne(ucfirst($form['paraOne']->getData()));   
            $groupe->setName(ucfirst($groupe->getName()));
            $groupe->setAlbumiframe(ucfirst($groupe->getAlbumiframe()));
            $em->persist($groupe);
            $em->flush();
            return $this->redirectToroute('add_groupe');
        }
        return $this->render('admin/groupe/groupe.html.twig', [
            'title' => 'Groupe',
            'formgroupe' => $form->createView(),
            'groupes' => $groupes,
            'json' => $json,
            'path' => $path
        ]);
    }

    /**
     * @Route("/add/concert", name="add_concert")
     * @Route("/update/concert/{id}", name="update_concert")
     */
    public function createConcert(Concert $concert = null, Request $request, ObjectManager $em, ParameterBagInterface $params, FileUploader $fileUploader){


        $concerts =  $em->getRepository(Concert::class)
        ->findAll();
        $json = $this->JsonBackground($concerts); // convert all pictures to Json
        $path = $params->get('upload_directory');
        
        if(!$concert){
            $concert = new Concert();
            $del_file=null;
        } else {
          $del_file = $concert->getPicture();
        }

        $form = $this->createForm(ConcertType::class, $concert);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()){
            if (!$del_file){
                $this->delFile('pictures', $del_file);
            }

            $pictureFile = $form['picture']->getData();
            $picture = $fileUploader->upload($pictureFile);
            $concert->setName(ucfirst($concert->getName()));
            $concert->setCity(ucfirst($concert->getCity()));
            $concert->setPlace(ucfirst($concert->getPlace()));
            $concert->setCp(ucfirst($concert->getCp()));
            $concert->setPicture($picture);
            $concert->setPicturealt(ucfirst($concert->getPictureAlt()));
            $em->persist($concert);
            $em->flush();
            
        return $this->redirectToroute('add_concert');
        }

        return $this->render('admin/concert/concert.html.twig', [
            'title' => 'Concert',
            'formconcert' => $form->createView(),
            'concerts' => $concerts,
            'json' => $json,
            'path' => $path
            
        ]);
    }
     /**
     * @Route("/soundtrack/add", name="add_soundtrack")
     * @Route("/soundtrack/update/{id}", name="update_soundtrack")
     */
    public function createSoundtrack(Soundtrack $soundtrack = null, Request $request, ObjectManager $em, SoundUploader $soundUploader){
    
        $soundtracks =  $em->getRepository(Soundtrack::class)
        ->findAll();

        if (!$soundtrack) {
            $del_file = null;
            $soundtrack = new Soundtrack();;
            } else {
            $del_file = $soundtrack->getSoundFileName();
            }
    
        $form = $this->createForm(SoundtrackType::class, $soundtrack);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $soundFile = $form['soundFileName']->getData();
            // ini_set('memory_limit', '8192M');
            $sound = $soundUploader->upload($soundFile);
            if (!is_null($del_file)){
                $this->delFile('musics', $del_file);
            }
            $soundtrack->setName(ucfirst($form['name']->getData()));
            $soundtrack->setSoundFileName($sound);
            $em->persist($soundtrack);
            $em->flush();
            return $this->redirectToroute('add_soundtrack');
        }

        return $this->render('admin/soundtrack/soundtrack.html.twig', [
            'title' => 'Soundtrack',
            'form' => $form->createView(),
            'soundtracks' => $soundtracks,
        ]);
    }
     /**
     * @Route("/soundOptions/add", name="add_soundOptions")
     * @Route("/soundOptions/update/{id}", name="update_soundOption")
     */
    public function soundOptions(SoundPosition $soundOptions = null, Request $request, ObjectManager $em, SoundUploader $soundUploader){
    
        $sound =  $em->getRepository(SoundPosition::class)
        ->findAll();

        if (!$soundOptions) {
            $del_file = null;
            $soundOptions = new SoundPosition();;
            } else {
            }
        $form = $this->createForm(SoundAlbumType::class, $soundOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            
            $soundOptions->setPosition($form['position']->getData());
            $soundOptions->setSoundtrack($form['soundtrack']->getData());
            $soundOptions->setAlbum($form['album']->getData());
            $em->persist($soundOptions);
            $em->flush();
            return $this->redirectToroute('add_soundOptions');
        }

        return $this->render('admin/soundtrack/soundAlbum.html.twig', [
            'title' => 'Soundtrack',
            'form' => $form->createView(),
            'soundtracks' => $sound,
            
        ]);
    }

     /**
     * @Route("/instrument/add", name="add_instrument")
     * @Route("/instrument/update/{id}", name="update_instrument")
     */
    public function createInstrument(Instrument $instrument = null, Request $request, ObjectManager $em){

        $instruments = $em->getRepository(Instrument::class)
        ->findAll();

        if(!$instrument) {
        $instrument = new Instrument(); 
        } else {
        
        }

        $form = $this->createForm(InstrumentType::class, $instrument);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $instrument->setName(ucfirst($instrument->getName()));
            $em->persist($instrument);
            $em->flush();
            return $this->redirectToroute('add_instrument');
        }

        return $this->render('admin/instrument/instrument.html.twig', [
            'title' => 'Instrument',
            'form' => $form->createView(),
            'instruments' => $instruments
        ]);
    }
     /**
     * @Route("/font/add", name="add_font")
     * @Route("/font/update/{id}", name="update_font")
     */
    public function addFont(Font $font = null, Request $request, ObjectManager $em){

        $fonts = $em->getRepository(Font::class)->findAll();

        if(!$font) {$font = new Font(); 
        } else {
        }

        $form = $this->createForm(FontFormType::class, $font);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $font->setGoogleFontUrl(ucfirst($font->getGoogleFontUrl()));
            $font->setGoogleFontUrl($font->getGoogleFontUrl());
            $em->persist($font);
            $em->flush();
            return $this->redirectToroute('add_font');
        }

        return $this->render('admin/fontGoogle/font.html.twig', [
            'title' => 'Google Font',
            'form' => $form->createView(),
            'fonts' => $fonts,
        ]);
    }
     /**
     * @Route("/play/add", name="add_play")
     * @Route("/play/update/{id}", name="update_play")
     */
    public function createPlays(Play $play = null, Request $request, ObjectManager $em){

        $plays = $em->getRepository(Play::class)
        ->findAll();

        if(!$play) {
        $play = new Play(); 
        }
        $form = $this->createForm(PlayType::class, $play);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $play->setArtist($form['artist']->getData());
            $play->setInstrument($form['instrument']->getData());
            $play->setSoundtrack($form['soundtrack']->getData());
            $em->persist($play);
            $em->flush();
            return $this->redirectToroute('add_play');
        }

        return $this->render('admin/play/play.html.twig', [
            'title' => 'Play',
            'form' => $form->createView(),
            'plays' => $plays
        ]);
    }

      /**
     *@Route("/delete/artist/{id}", name="del_art") 
    */
    public function delArtist(Artist $artist, ObjectManager $em){
        $em->remove($artist);
        $em->flush();
        return $this->redirectToRoute('add_artist');  
        }

    /**
     *@Route("/delete/groupe/{id}", name="del_groupe") 
     */
    public function delGroupe(Groupmusic $groupe, ObjectManager $em){

        $file = $groupe->getPicture();
        $this->delFile('pictures', $file);
        $em->remove($groupe);
        $em->flush();
        return $this->redirectToRoute('add_groupe');  
    }

    /**
     *@Route("/delete/concert/{id}", name="del_concert") 
     */
    public function delConcert(Concert $concert, ObjectManager $em){

        $file = $concert->getPicture();
        $this->delFile('pictures', $file);
        $em->remove($concert);
        $em->flush();
        return $this->redirectToRoute('add_concert');  
    }
    /**
     *@Route("/delete/news/{id}", name="del_news") 
     */
    public function delNews(News $news, ObjectManager $em){

        $file = $news->getPicture();
        $this->delFile('pictures', $file);
        $em->remove($news);
        $em->flush();
        return $this->redirectToRoute('add_news');  
    }

    /**
     *@Route("/delete/album/{id}", name="del_album") 
     */
    public function delAlbum(Album $album, ObjectManager $em){

        $file = $album->getPicture();
        $this->delFile('pictures', $file);
        $em->remove($album);
        $em->flush();
        return $this->redirectToRoute('add_album');  
    }
    /**
     *@Route("/delete/play/{id}", name="del_play") 
     */
    public function delPlay(Play $play, ObjectManager $em){

        $em->remove($play);
        $em->flush();
        return $this->redirectToRoute('add_album');  
    }
    /**
     *@Route("/delete/soundtrack/{id}", name="del_soundtrack") 
     */
    public function delSoundtrack(Soundtrack $soundtrack, ObjectManager $em){

        $file = $soundtrack->getSoundFileName();
        $this->delFile('musics', $file);
        $em->remove($soundtrack);
        $em->flush();
        return $this->redirectToRoute('add_soundtrack');  
    }
    /**
     *@Route("/delete/label/{id}", name="del_label") 
     */
    public function del_label(Label $label, ObjectManager $em){

        $em->remove($label);
        $em->flush();
        return $this->redirectToRoute('add_label');  
    }
    /**
     *@Route("/delete/instrument/{id}", name="del_instrument") 
     */
    public function delInstrument(Instrument $instrument, ObjectManager $em){

        $em->remove($instrument);
        $em->flush();
        return $this->redirectToRoute('add_instrument');  
    }
  
    /**
     *@Route("/delete/font/{id}", name="del_font") 
     */
    public function delFont(Font $font, ObjectManager $em){

        $em->remove($font);
        $em->flush();
        return $this->redirectToRoute('add_font');  
    }
  
    
    public function delFile($dir, $del_file){
        $fsObject = new Filesystem();
        $current_dir_path = getcwd();
            $delTarget = $current_dir_path . "/assets/". $dir ."/". $del_file;
            if($del_file){
               return $fsObject->remove($delTarget);
            }
    }

    public function JsonBackground($tab){
        $pictures = []; 
        foreach($tab as $newsJson){
            array_push($pictures, $newsJson->getPicture());
        } 
        $json = json_encode($pictures);
        return $json;

    }

    /**
     *@Route("/facture/{id}", name="generate_pdf") 
    */
    public function generatePdf(){
        $dompdf = new Dompdf();
        $dompdf->loadHtml('hello world');
        
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');
        
        // Render the HTML as PDF
        $dompdf->render('');
        
        // Output the generated PDF to Browser
        return $dompdf->stream();

    }
   
}
