<?php

namespace App\Controller\Admin;

use App\Entity\Archive;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\Shipping;
use App\Form\ProductType;
use App\Form\CategoryType;
use App\Service\FileUploader;
use App\Controller\Admin\AdminController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @Route("admin/shop")
 */
class ShopController extends AbstractController{

    /**
     * @Route("/product/add", name="add_product")
     * @Route("/product/edit/{id}", name="edit_product")
     */
    public function addProducts(Product $product = null, ObjectManager $em, Request $request,FileUploader $fileUploader, ParameterBagInterface $params){

    $admin = new AdminController();
    $products = $em->getRepository(Product::class)->findAll();
    $json = $admin->JsonBackground($products); // convert all pictures to Json
    $path = $params->get('upload_directory'); // path to JS
        
   
    if (!$product) { $del_file = null; $product = new Product();; //Add-Edit Test
    } else { $del_file = $product->getPicture();
    }

    $form = $this->createForm(ProductType::class, $product);
    $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $pictureFile = $form['picture']->getData();
             if (!is_null($del_file)){
                $admin->delFile('pictures', $del_file);
            }

            $picture = $fileUploader->upload($pictureFile);
            $product->setPicture($picture);
            $product->setName(ucfirst($form['name']->getData()));
            $product->setDescribes(ucfirst($form['describes']->getData()));
            $product->setPrice($form['price']->getData());
            $product->setQty($form['qty']->getData());
            $em->persist($product);
            $em->flush();
            
        return $this->redirectToRoute('add_product');
        }

    return $this->render('admin/shop/shop.html.twig', [
        'title' => 'Shop',
        'form' => $form->createView(),
        'products' => $products,
      
    ]);
    }


    /**
     * @Route("/product/category", name="add_category_product")
     */
    public function addCategory(Category $category = null, ObjectManager $em, Request $request ){

            $category = new Category();
            $form = $this->createForm(CategoryType::class, $category);
            $form->handleRequest($request);

                if($form->isSubmitted() && $form->isValid()){
                $category->setName(ucfirst($form['name']->getData()));
                $em->persist($category);
                $em->flush();
                return $this->redirectToRoute('add_category_product');
            }
            return $this->render('admin/shop/category.html.twig',[
                'form' => $form->createView(),
            ]);
    }
    
    /**
     * @Route("/product/commande", name="show_order")
     */
    public function showOrder(ObjectManager $em){

        $commandes = $em->getRepository(Shipping::class)->findAll();
        foreach($commandes as $commande){
            $commande->setProducts($commande->getProducts());
            $commande->setShip($commande->getShip());
        }
        return $this->render('admin/shop/order.html.twig',[
                'commandes' => $commandes,
            ]);
    }
    

    /**
     * @Route("/product/del/{id}", name="del_product")
     */
    public function delProduct(Product $product, ObjectManager $em){
            $em->remove($product);
            $em->flush();
            return $this->redirectToRoute('add_product');
    }

    /**
     * @Route("/product/del/{id}", name="del_category")
     */
    public function delCategory(Category $category, ObjectManager $em){
            $em->remove($category);
            $em->flush();
            return $this->redirectToRoute('add_category_product');
    }

    /**
     *@Route("/archive/{id}", name="archive_product") 
    */
    public function createArchive(Shipping $shipping, ObjectManager $em){

        $shipping = $em->getRepository(Shipping::class)->find($shipping);
        
        $archive = new Archive();
        $archive->setEmail($shipping->getEmail());
        $archive->setPrice($shipping->getPrice());
        $archive->setCurrency($shipping->getCurrency());
        $archive->setProducts($shipping->getProducts());
        $archive->setCreateat(new \DateTime('now'));
        $archive->setUniqueId($shipping->getUniqueId());
        $archive->setCurrency($shipping->getCurrency());
        $em->persist($archive);
       
        $em->remove($shipping);
        $em->flush();
 
        $this->addFlash('notice', 'Commande envoyée et archivée.');
        return $this->redirectToRoute('show_order');
 
     }


    /**
     *@Route("/archives/", name="show_archive") 
    */
    public function showArchives( ObjectManager $em){

        $archives = $em->getRepository(Archive::class)->findAll();
      
     
        return $this->render('admin/shop/archive.html.twig ', [
            'archives' => $archives
        ]);
    }
 

}
