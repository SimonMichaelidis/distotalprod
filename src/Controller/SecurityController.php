<?php
namespace App\Controller;

use DateTime;
use DateInterval;
use App\Entity\User;
use App\Form\UserType;
use App\Service\SessionService;
use App\Service\MailChimpApiConnect;
use App\Controller\HaveALoginController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class SecurityController extends HaveALoginController {

   
    /**
     * @Route("/logout", name="logout")
     */
    public function logoutMessage(SessionService $session)
    {
        $this->addFlash('success', 'Vous êtes déconnecté(e)');
        return $this->redirectToRoute('home_index');
    }

    /**
    * @Route("/forgotten_password", name="forgotten_password")
    */
   public function forgottenPassword(Request $request, ObjectManager $em, UserPasswordEncoderInterface $encoder,\Swift_Mailer $mailer,
  TokenGeneratorInterface $tokenGenerator){

    if ($request->isMethod('POST')) {
        $email = $request->request->get('email');
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->findOneBy(['email' => $email]);

    if ($user === null) {
        $this->addFlash('danger', 'Email Inconnu');
        return $this->redirectToRoute('home_index');
    }
    $token = $tokenGenerator->generateToken();

    try{
        $user->setResetToken($token);
        $date = new \DateTime();
        $date->add(new \DateInterval('P1D'));
        $user->setDateValidToken($date);
        
        $entityManager->flush();
    } catch (\Exception $e) {
        $this->addFlash('warning', $e->getMessage());
        return $this->redirectToRoute('shop');
    }
        $url = $this->generateUrl('app_reset_password', [
            'token' => $token
        ], UrlGeneratorInterface::ABSOLUTE_URL);
        $message = (new \Swift_Message('Forgotten Password'))
            ->setFrom('DistotalProduction@gmail.fr')
            ->setTo($user->getEmail())
            ->setBody(
                "Voici le token pour confirmer votre mot de passe : " . $url,
                'text/html'
            );
        $mailer->send($message);
        $this->addFlash('notice', 'Vous allez recevoir un email afin de confirmer vos nouveaux identifiants.');
        return $this->redirectToRoute('shop');
    }
    return $this->render('security/forgotten_password.html.twig');
}

    /**
     * @Route("/reset_password/{token}", name="app_reset_password")
     */
    public function resetPassword(Request $request, string $token, UserPasswordEncoderInterface $passwordEncoder)
    {

        if ($request->isMethod('POST')) {
            $entityManager = $this->getDoctrine()->getManager();
            $user = $entityManager->getRepository(User::class)->findOneBy(['resetToken' => $token]);
            $dateToken = $entityManager->getRepository(User::class)->findOneBy(['resetToken' => $token]);
            /* @var $user User */
            if ($user === null) {
                $this->addFlash('notice', 'Token Inconnu');
                return $this->redirectToRoute('shop');
            }
            $tokenValid = $user->getDateValidToken();
            $date = new DateTime('now');
            if($date > $tokenValid){
                $this->addFlash('notice', 'Date de validation du Token expirée');
            }
            $user->setResetToken(null);
            $user->setDateValidToken(null);
            $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
            $entityManager->flush();
            $this->addFlash('notice', 'Mot de passe mis à jour');
            return $this->redirectToRoute('shop');
        }else {
            return $this->render('security/reset_password.html.twig', ['token' => $token]);
        }

    }

    /**
     * @Route("/register", name="register")
     */
    public function registerAction(SessionService $session, Request $request, UserPasswordEncoderInterface $passwordEncoder,  \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator, ObjectManager $em) {

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $dateValidAuthentication = new \DateTime('now');
            $dateValidAuthentication->add(new \DateInterval('PT8M'));
            $token = $tokenGenerator->generateToken();
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setResetToken($token);
            $user->setDateValidToken($dateValidAuthentication);
            $user->setIsActive(false);
            $user->setPassword($password);
            $em->persist($user);
            $em->flush();
            
            $session = $this->get('session');
            $session->set('validAuthentication', $dateValidAuthentication);
            $session->set('firstname', $user->getFirstname());
            $session->set('lastname', $user->getLastname());

            $url = $this->generateUrl('confirm_new_account', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);
            
            $message = (new \Swift_Message('Bienvenue chez Distotal Prod !'))
            ->setFrom('michaelidis.simon06@gmail.com')
            ->setTo($form['email']->getData())
            ->setBody(
                $this->renderView(
                    'emails/registration.html.twig',[
                        'name' => 'michaelidis.simon06@gmail.com',
                        'url' => $url,
                    ]
                ),
                'text/html'
            );
            $mailer->send($message);
            $this->addFlash('notice', 'Vous allez recevoir un email de confirmation de compte.');
            return $this->redirectToRoute('shop');  
        }
        return $this->render('registration/register.html.twig', ['form' => $form->createView(), 'mainNavRegistration' => true, 'title' => 'Inscription']);
    }

    /**
     * @Route("/confirmAccount/{token}", name="confirm_new_account")
     */
        public function confirmAccount(SessionService $session, Request $request, string $token, ObjectManager $em){

        $user = $em->getRepository(User::class)->findOneBy(['resetToken' => $token]);
        $tokenValid = $user->getResetToken();
        $dateTokenValid = $user->getDateValidToken();
        $now = new DateTime('now');
        $session = $request->getSession();
    
        if ($user === null) {
            $em->remove($user);
            $em->flush();
            $this->addFlash('notice', 'Token inconnu, veuillez recommencer l\'opération.');
            return $this->redirectToRoute('shop');
        }
        if ($dateTokenValid < $now){
            $em->remove($user);
            $em->flush();
            $this->addFlash('notice', 'Token expiré.');
            return $this->redirectToRoute('shop');
        } else {
            $user->setFirstname($session->get('firstname'));
            $user->setLastname($session->get('lastname'));
            $user->setIsActive(true);
            $user->addRole("ROLE_USER");
            $user->setResetToken(null);
            $user->setDateValidToken(null);
            $em->flush($user);
            $this->addFlash('notice', 'Votre compte est enregistré.');
            return $this->redirectToRoute('cart');
        }

}


    /**
     * @Route("/updateuser/", name="update_user", methods={"POST"})
     */

     public function updateUser(SessionService $session, Request $request, ObjectManager $em, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator){

        $submittedToken = $request->request->get('token');
        if ($this->isCsrfTokenValid('authenticate', $submittedToken)){
        $user = $this->getUser();
        } else {
            return $this->redirectToRoute('home_index');
        }

      
        $mchimp = new MailChimpApiConnect();
        if (isset($_POST['newsletter'])){
            if($_POST['newsletter'] == "on"){
                $api = $mchimp->getApi();
                $target =  "lists/".$mchimp->getIdList()."/members/". md5($this->getUser()->getEmail());
                $data = [
                    'email_address' => $this->getUser()->getEmail(),
                    'send_welcome'  => true,
                    'status' => 'subscribed', 
                ];
                $mchimp->mc_request($api, 'PUT', $target, $data);
            } 
        } else {
            $id = $this->getUser()->getEmail();
            $api = $mchimp->getApi();
            $target =  "lists/".$mchimp->getIdList()."/members/".md5($id);
            $mchimp->mc_request($api, 'DELETE', $target);

        }
            
        if(isset($_POST['nom'])){
        $user->setLastname($_POST['nom']);
        }
        if(isset($_POST['prenom'])){
        $user->setFirstname($_POST['prenom']);
        }
        if(isset($_POST['email'])){

            $dateValidAuth = new \DateTime('now');
            $dateValidAuth->add(new \DateInterval('PT8M'));

            $session = new Session();
            $session->set('email', $_POST['email']);
            $session->set('dateValidAuth', $dateValidAuth);

            $token = $tokenGenerator->generateToken();
            $user->setResetToken($token);
            $em->persist($user);
            $em->flush();

            $url = $this->generateUrl('confirm_update_email', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);
            $message = (new \Swift_Message('Confirmation de votre nouvelle adresse e-mail.'))
            ->setFrom('michaelidis.simon06@gmail.com')
            ->setTo('michaelidis.simon06@gmail.com')
            ->setBody(
                $this->renderView(
                    'emails/update_email.html.twig',[
                        'name' => 'michaelidis.simon06@gmail.com',
                        'url' => $url,
                    ]
                ),
                'text/html'
            );
            $mailer->send($message);

        }
      
        $this->addFlash('notice', 'Vous allez recevoir un email de confirmation.');
        return $this->redirectToRoute('shop');
        } 

    /**
     * @Route("/confirm_update_email/{token}", name="confirm_update_email")
     */
    public function update_email(SessionService $session, Request $request, ObjectManager $em, string $token){

      $user = $em->getRepository(User::class)->findOneBy(['resetToken' => $token]);

      $session = $request->getSession();

      if ($user == null){
        $this->addFlash('notice', "Token non6valide");
        return $this->redirectToRoute('shop');
      }
    
      if (new \DateTime('now') > $session->get('dateValidAuth')){
        $user->setResetToken(null);
        $em->persist($user);
        $em->flush();
      $this->addFlash('notice', "Token expiré");
      return $this->redirectToRoute('shop');
      }

      $user->setEmail($session->get('email'));
      $user->setResetToken(null);
      $em->persist($user);
      $em->flush();


      $this->addFlash('notice', 'Adresse e-mail mise à jour');
      return $this->redirectToRoute('shop');

      }
      
        /**
        *@Route("/user_delete/", name="delete_user") 
        */
        public function delUser(ObjectManager $em, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator){
           
            $user = $this->getUser();
            $token = $tokenGenerator->generateToken();

            $dateValidAuth = new \DateTime('now');
            $dateValidAuth->add(new \DateInterval('PT8M'));
        
            $user->setResetToken($token);
            $user->setDateValidToken($dateValidAuth);
            $em->persist($user);
            $em->flush();

            $url = $this->generateUrl('confirm_delete_user', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);
            $message = (new \Swift_Message('Confirmation de la suppression de votre compte'))
            ->setFrom('michaelidis.simon06@gmail.com')
            ->setTo('michaelidis.simon06@gmail.com')
            ->setBody(
                $this->renderView(
                    'emails/delete_account.html.twig',[
                        'name' => 'michaelidis.simon06@gmail.com',
                        'url' => $url,
                    ]
                ),
                'text/html'
            );
            $mailer->send($message);
            $this->addFlash('notice', "Vous allez recevoir un email de confirmation");
            return $this->redirectToRoute('logout'); 
        }

    /**
     * @Route("/confirm_delete_user/{token}", name="confirm_delete_user")
     */
    public function confirmDeleteUser(Request $request, ObjectManager $em, string $token){
        
    $user = $em->getRepository(User::class)->findOneBy(['resetToken' => $token]);

    if ($user == null){
        $this->addFlash('notice', "Token non valide");
        return $this->redirectToRoute('shop');
    }
 
    if (new \DateTime('now') > $user->getDateValidToken()){
        $this->addFlash('notice', "Token expiré");
        return $this->redirectToRoute('shop');
    }

    $em->remove($user);
    $em->flush();
    $this->addFlash('notice', "Compte supprimé");
    return $this->redirectToRoute('shop');

    }
}
    
    


     
   