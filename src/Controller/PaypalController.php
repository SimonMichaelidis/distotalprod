<?php

namespace App\Controller;

use App\Service\Paypal;
use App\Service\SessionService;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PaypalController extends AbstractController
{
    
public function __construct(Paypal $pp){
    $this->pp = $pp;
}
   

    /**
     * @Route("/create_payment", name="create_payment", methods={"POST"})
     */ 
    public function createPayment(SessionService $session){
    $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
    if(!$session->session->has('cart') || empty($session->session->get('cart')) || $session->session->get('cart_data/total_price') == 0){    
    return $this->redirectToRoute('shop');
    }
    return $this->redirect($this->pp->createPayment());
    

    }


     /**
     * @Route("/execute_payment/", name="execute_payment")
     */
    public function executePayment(ObjectManager $em){
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $this->pp->executePayment($em);
        $this->addFlash("notice", "Achat effectué. Nous préparons votre commande");
        return $this->redirectToRoute('shop');

    }
}
