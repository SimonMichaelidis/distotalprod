<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Category;
use App\Service\SessionService;
use App\Service\ShoppingCartService;
use App\Controller\HaveALoginController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ShoppingCartController extends HaveALoginController{

     /**
     * @Route("/shop", name="shop")
     */
    public function showProducts(ObjectManager $em, Request $request, ParameterBagInterface $params, AuthenticationUtils $authenticationUtils ){

    
        $products = $em->getRepository(Product::class)->findAll();
        $category = $em->getRepository(Category::class)->findAll();
       
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $form = $this->showLogin($request, $authenticationUtils);

        return $this->render('select/shop.html.twig', [
            'products' => $products,
            'category' => $category,
            'formLogin' => $form->createView(),
           
         ]);
    }

    /**
     * @Route("/cart", name="cart")
     */
    public function cart(SessionService $session,Request $request, AuthenticationUtils $authenticationUtils){

        $this->getUser() ? $toggleNav = 1 : $toggleNav = 0;

        if ($session->session->get('cart', [])){
            $cart_products = $session->session->get('cart', []);
            $total_price = $session->session->get('cart_data/total_price', 0);
            $total_amount = $session->session->get('cart_data/total_amount', 0);
        
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $form = $this->showLogin($request, $authenticationUtils);

            return $this->render('select/cart.html.twig', [
                'formLogin' => $form->createView(),
                'cart_products' => $cart_products,
                'total_price' => $total_price,
                'total_amount' => $total_amount,
                'toggle' => json_encode($toggleNav)
            ]);
        } else 
        $this->addFlash('notice', 'Votre panier est vide');
        return $this->redirectToRoute('shop');
    }

    /**
     * @Route("/remove_from_cart/{id}", name="remove_from_cart")
     */
    public function removeFromCart(Product $product, ShoppingCartService $cart){
    $cart->removeFromCart($product->getId());
    return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/update_quantities", name="update_quantities")
     */
    public function updateQuantities(Request $request, ShoppingCartService $cart){
        if($request->request->get('amounts'))
        $cart->updateQuantities($request);
        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/add_to_cart/{id}", name="add_to_cart")
     */
    public function addToCart(Product $product, ShoppingCartService $cart){
        $cart->addToCart($product);
        return $this->redirectToRoute('shop');
    }

    public function deleteCart($session){
        $this->session->remove('cart');
        $this->session->remove('cart_data');
        // $this->session->clear();
        return $this->redirectToRoute('shop');
    }

  
}
