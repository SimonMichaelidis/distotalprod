<?php

namespace App\Repository;

use App\Entity\Bandcamp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Bandcamp|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bandcamp|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bandcamp[]    findAll()
 * @method Bandcamp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BandcampRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bandcamp::class);
    }

    // /**
    //  * @return Bandcamp[] Returns an array of Bandcamp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bandcamp
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
