<?php

namespace App\Repository;

use App\Entity\Soundtrack;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Soundtrack|null find($id, $lockMode = null, $lockVersion = null)
 * @method Soundtrack|null findOneBy(array $criteria, array $orderBy = null)
 * @method Soundtrack[]    findAll()
 * @method Soundtrack[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SoundtrackRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Soundtrack::class);
    }

    // /**
    //  * @return Soundtrack[] Returns an array of Soundtrack objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Soundtrack
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
