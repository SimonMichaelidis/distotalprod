<?php

namespace App\Repository;

use App\Entity\Groupmusic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Groupmusic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Groupmusic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Groupmusic[]    findAll()
 * @method Groupmusic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupmusicRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry){
        parent::__construct($registry, Groupmusic::class);
    }

    // select name describe album

   



    // /**
    //  * @return Groupmusic[] Returns an array of Groupmusic objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Groupmusic
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
