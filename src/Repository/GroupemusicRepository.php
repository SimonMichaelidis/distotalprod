<?php

namespace App\Repository;

use App\Entity\Groupemusic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Groupemusic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Groupemusic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Groupemusic[]    findAll()
 * @method Groupemusic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupemusicRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Groupemusic::class);
    }

    // /**
    //  * @return Groupemusic[] Returns an array of Groupemusic objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Groupemusic
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
