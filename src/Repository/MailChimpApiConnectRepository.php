<?php

namespace App\Repository;

use App\Entity\MailChimpApiConnect;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MailChimpApiConnect|null find($id, $lockMode = null, $lockVersion = null)
 * @method MailChimpApiConnect|null findOneBy(array $criteria, array $orderBy = null)
 * @method MailChimpApiConnect[]    findAll()
 * @method MailChimpApiConnect[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MailChimpApiConnectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MailChimpApiConnect::class);
    }

    // /**
    //  * @return MailChimpApiConnect[] Returns an array of MailChimpApiConnect objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MailChimpApiConnect
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
