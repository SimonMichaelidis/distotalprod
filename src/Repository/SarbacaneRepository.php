<?php

namespace App\Repository;

use App\Entity\Sarbacane;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Sarbacane|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sarbacane|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sarbacane[]    findAll()
 * @method Sarbacane[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SarbacaneRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Sarbacane::class);
    }

    // /**
    //  * @return Sarbacane[] Returns an array of Sarbacane objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sarbacane
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
