<?php

namespace App\Repository;

use App\Entity\FacebookService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FacebookService|null find($id, $lockMode = null, $lockVersion = null)
 * @method FacebookService|null findOneBy(array $criteria, array $orderBy = null)
 * @method FacebookService[]    findAll()
 * @method FacebookService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacebookServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FacebookService::class);
    }

    // /**
    //  * @return FacebookService[] Returns an array of FacebookService objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FacebookService
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
